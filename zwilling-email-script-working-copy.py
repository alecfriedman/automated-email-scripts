new_file = 'new_zwilling_email.html'
with open(new_file, 'w') as file_object:
	file_object.write('''
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Zwilling J.A.Henckels</title>
    
  <style type="text/css">
		html,body{
			margin:0;
			padding:0;
			background:#FFFFFF;
		}
	@media screen and (max-width:1024px){
		html,body{
			-webkit-text-size-adjust:none;
		}

}		a{
			text-decoration:none;
			border:0;
			outline:0;
		}
		a.nav-link{
			color:#ffffff;
			padding:0;
			font-size:10px;
			text-transform:uppercase;
			font-family:Helvetica,Arial,sans-serif;
		}
		p{
			font-family:Arial,Helvetica,sans-serif;
			font-size:16px;
			margin-bottom:20px;
		}
		img{
			border:0;
			outline:0;
			display:block;
		}
</style></head>
  <body>
    <!-- email subject line	-->
    <!--*|IF:MC_PREVIEW_TEXT|*-->
    <!--[if !gte mso 9]><!--><span class="mcnPreview Text" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;">*|MC_PREVIEW_TEXT|*</span>
    <!--<![endif]-->
    <!--*|END:IF|*-->
    <!--	end email subject line	-->
    
    <table width="580" cellpadding="0" cellspacing="0" border="0" align="center" style="min-width:580px;border:1px solid #000;">
''')

# Creating a section class
class Section:
	def __init__(self, width, height, image, link):
		self.width = width
		self.height = height
		self.image = image
		self.link = link
		with open(new_file, 'a') as file_object:
			file_object.write('''
							<td>
								<div style="height:'''+self.height+'''px;">
									<a href="'''+self.link+'''">
										<img src="'''+self.image+'''" width="'''+self.width+'''" height="'''+self.height+'''" border="0" alt="">
									</a>
								</div>
							</td>''')

# Creating a footer class
class Footer:
	def __init__(self, face, manicure, grooming, new):
		self.face = face
		self.manicure = manicure
		self.grooming = grooming
		self.new = new
		with open(new_file, 'a') as file_object:
			file_object.write('''
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td>
								<div style="height:77px;">
									<a href="'''+self.face+'''" target="_blank">
										<img src="https://gallery.mailchimp.com/63aca8e9028b75ea575d5baee/images/202a7f80-414a-40c9-8f6b-17c6f1111607.gif" width="580" height="77" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td>
								<div style="height:76px;">
									<a href="'''+self.manicure+'''" target="_blank">
										<img src="https://gallery.mailchimp.com/63aca8e9028b75ea575d5baee/images/f62c05d8-1643-41e5-b610-c50a79590008.gif" width="580" height="76" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td>
								<div style="height:78px;">
									<a href="'''+self.grooming+'''" target="_blank">
										<img src="https://gallery.mailchimp.com/63aca8e9028b75ea575d5baee/images/93b1e008-a559-4009-8d2c-337840b6cfe4.gif" width="580" height="78" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td>
								<div style="height:81px;">
									<a href="'''+self.new+'''" target="_blank">
										<img src="https://gallery.mailchimp.com/63aca8e9028b75ea575d5baee/images/45f71046-df00-4ab6-8108-9ac0afa70bf7.gif" width="580" height="81" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td>
								<div style="height:120px;">
									<a href="#">
										<img src="https://gallery.mailchimp.com/59412780d3a46e1a69c084c5e/images/e9a2c447-cba6-453c-9b95-1496ab53a0f3.png" width="580" height="120" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td style="width:330px;">
								<div style="height:59px;">
									<a href="#">
										<img src="https://gallery.mailchimp.com/59412780d3a46e1a69c084c5e/images/33e60b98-8599-43c7-85d2-ac99c5126a1c.png" width="330" height="59" border="0" alt="">
									</a>
								</div>
							</td>
							<td style="width:47px;">
								<div style="height:59px;">
									<a href="https://www.facebook.com/ZwillingBeautyUSA/" target="_blank">
										<img src="https://gallery.mailchimp.com/59412780d3a46e1a69c084c5e/images/6ac83b1c-eaa6-4881-b5a2-b62d5a5e76a4.png" width="47" height="59" border="0" alt="">
									</a>
								</div>
							</td>
							<td style="width:57px;">
								<div style="height:59px;">
									<a href="https://www.instagram.com/zwillingbeautyusa/" target="_blank">
										<img src="https://gallery.mailchimp.com/59412780d3a46e1a69c084c5e/images/1367d9eb-adb7-4cac-8f58-e220dd4fad38.png" width="57" height="59" border="0" alt="">
									</a>
								</div>
							</td>
							<td style="width:146px;">
								<div style="height:59px;">
									<a href="#">
										<img src="https://gallery.mailchimp.com/59412780d3a46e1a69c084c5e/images/5925770a-2c56-4629-b92c-0bf4b143601e.png" width="146" height="59" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<td>
		<table width="580" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
							<td>
								<div style="height:45px;">
									<a href="#">
										<img src="https://gallery.mailchimp.com/59412780d3a46e1a69c084c5e/images/2b1b7557-91fc-4180-ad77-7dabedeb8392.png" width="580" height="45" border="0" alt="">
									</a>
								</div>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>
''')


new_section = input('Press enter to add a new section or type quit to finish: ')
while new_section != 'quit':
	with open(new_file, 'a') as file_object:
		file_object.write('''
<tr>
	<td>
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
''')	
	columns = input('Enter the amount of columns or leave blank for 1: ')
	while not columns.isdigit():
		if not columns:
			columns = 1
			break
		columns = input('Enter the amount of columns or leave blank for 1: ')
	column_count = int(columns)
	column_status = 1
	while column_status <= column_count:
		print('''
new column '''+str(column_status)+'''!
''')
		width = input('Enter the width in pixels of the image you want to display: ')
		while not width.isdigit():
			width = input('Enter the width in pixels of the image you want to display: ')
		height = input('Enter the height in pixels of the image you want to display: ')
		while not height.isdigit():
			height = input('Enter the height in pixels of the image you want to display: ')
		image = input('Paste the URL of the image you want to display: ')
		while not image:
			image = input('Paste the URL of the image you want to display: ')
		link = input('URL you want to link to, leave blank if no link: ')
		created_section = Section(width, height, image, link)
		column_status += 1
	with open(new_file, 'a') as file_object:
			file_object.write('''
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>''')
	print('''
Section Added! ... 


''')
	new_section = input('''
Add a new section or enter quit to finish: ''')
print('''
Footer Links''')
face = input("Face: ")
while not face:
	face = input("Face: ")
manicure = input("Manicure & Pedicure: ")
while not manicure:
	manicure = input("Manicure & Pedicure: ")
grooming = input("Grooming Sets: ")
while not grooming:
	grooming = input("Grooming Sets: ")
new = input("New Arrivals: ")
while not new:
	new = input("New Arrivals: ")
created_footer = Footer(face, manicure, grooming, new)

new_section = input('Press enter to add a new section or type quit to finish: ')
while new_section != 'quit':
	with open(new_file, 'a') as file_object:
		file_object.write('''
<tr>
	<td>
		<table width="100%" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="0" cellspacing="0" border="0" style="border-collapse:collapse;">
						<tr>
''')	
	columns = input('Enter the amount of columns or leave blank for 1: ')
	while not columns.isdigit():
		if not columns:
			columns = 1
			break
		columns = input('Enter the amount of columns or leave blank for 1: ')
	column_count = int(columns)
	column_status = 1
	while column_status <= column_count:
		print('''
new column '''+str(column_status)+'''!
''')
		width = input('Enter the width in pixels of the image you want to display: ')
		while not width.isdigit():
			width = input('Enter the width in pixels of the image you want to display: ')
		height = input('Enter the height in pixels of the image you want to display: ')
		while not height.isdigit():
			height = input('Enter the height in pixels of the image you want to display: ')
		image = input('Paste the URL of the image you want to display: ')
		while not image:
			image = input('Paste the URL of the image you want to display: ')
		link = input('URL you want to link to, leave blank if no link: ')
		created_section = Section(width, height, image, link)
		column_status += 1
	with open(new_file, 'a') as file_object:
			file_object.write('''
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
</tr>''')
	print('''
Section Added! ... 


''')
	new_section = input('''
Add a new section or enter quit to finish: ''')

with open(new_file, 'a') as file_object:
	file_object.write('''
 </table>
</body>
</html>
''')